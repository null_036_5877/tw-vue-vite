/*
 * @Description:
 * @Version:
 * @Autor: LiChuang
 * @Date: 2022-06-29 16:19:20
 * @LastEditors:
 * @LastEditTime: 2022-06-29 16:23:33
 */
// @ts-ignore
import path from 'path'
import {defineConfig} from 'vite'
import vue from '@vitejs/plugin-vue'
import vueJsx from '@vitejs/plugin-vue-jsx'
import Icons from 'unplugin-icons/vite'
import IconsResolver from 'unplugin-icons/resolver'
import AutoImport from 'unplugin-auto-import/vite'
import Components from 'unplugin-vue-components/vite'
import { viteMockServe } from 'vite-plugin-mock'
import { ElementPlusResolver } from 'unplugin-vue-components/resolvers'
import ReactivityTransform from '@vue-macros/reactivity-transform/vite'
    import cesium from 'vite-plugin-cesium';
// import Inspect from 'vite-plugin-inspect'

const resolve = path.resolve(__dirname, 'src')

export default defineConfig(({ command, mode }) => {
    if (command === 'serve') {
        // dev 独有配置
        return {
            server:{
                hmr:{
                    overlay: true    // 默认是 true，设置为false后报错不会在以vite的形式在页面显示了
                }
            },
            define: {
                'process.env': {
                    VUE_APP_BASE_API: ""
                }
            },
            base: './',
            resolve: {
                alias: {
                    '@': resolve,
                },
            },
            esbuild: {
                jsxFactory: 'h',
                jsxFragment: 'Fragment'
            },
            plugins: [
                vue({
                    reactivityTransform: true,
                }),
                cesium(),
                ReactivityTransform(),
                vueJsx({
                    // options are passed on to @vue/babel-plugin-jsx
                }),
                AutoImport({
                    imports: ['vue', 'vue-router', '@vueuse/core', 'vuex'],
                    dts: 'src/atuoFlies/auto-imports.d.ts',
                    resolvers: [
                        // // 自动导入图标组件
                        // IconsResolver({
                        //     prefix: 'Icon',
                        // }),
                        // ElementPlusResolver()
                    ],
                }),
                Components({
                    dirs: ['@/element-plus/es/components', 'src/components', 'tw-vue-el'],
                    dts: 'src/atuoFlies/components.d.ts',
                    resolvers: [
                        // // 自动注册图标组件
                        // IconsResolver({
                        //     enabledCollections: ['ep'],
                        // }),
                        // // 自动导入 Element Plus 组件
                        // ElementPlusResolver(),
                    ],
                }),
                viteMockServe({
                    mockPath: "src/mock", // 解析，路径可根据实际变动
                    localEnabled: true // 此处可以手动设置为true，也可以根据官方文档格式
                }),
                Icons({
                    autoInstall: true,
                }),
                // Inspect(),  // 页面流程分析图（会直接影响启动速度）
            ],
            // build: {
            //     // 在 outDir 中生成 manifest.json
            //     manifest: true,
            //     rollupOptions: {
            //         // 覆盖默认的 .html 入口
            //         input: '/path/to/main.js'
            //     }
            // }
        }
    } else {
        // command === 'build'
        return {
            // build 独有配置
            server:{
                hmr:{
                    overlay: true    // 默认是 true，设置为false后报错不会在以vite的形式在页面显示了
                }
            },
            define: {
                'process.env': {
                    VUE_APP_BASE_API: ""
                }
            },
            base: './',
            resolve: {
                alias: {
                    '@': resolve,
                },
            },
            esbuild: {
                jsxFactory: 'h',
                jsxFragment: 'Fragment'
            },
            plugins: [
                vue({
                    reactivityTransform: true,
                }),
                cesium(),
                vueJsx({
                    // options are passed on to @vue/babel-plugin-jsx
                }),
                AutoImport({
                    imports: ['vue', 'vue-router', '@vueuse/core', 'vuex'],
                    dts: 'src/atuoFlies/auto-imports.d.ts',
                    resolvers: [
                        // // 自动导入图标组件
                        // IconsResolver({
                        //     prefix: 'Icon',
                        // }),
                        // ElementPlusResolver()
                    ],
                }),
                Components({
                    dts: 'src/atuoFlies/components.d.ts',
                    resolvers: [
                        // // 自动注册图标组件
                        // IconsResolver({
                        //     enabledCollections: ['ep'],
                        // }),
                        // // 自动导入 Element Plus 组件
                        // ElementPlusResolver(),
                    ],
                }),
                viteMockServe({
                    mockPath: "src/mock", // 解析，路径可根据实际变动
                    localEnabled: true // 此处可以手动设置为true，也可以根据官方文档格式
                }),
                Icons({
                    autoInstall: true,
                }),
                // Inspect(),  // 页面流程分析图（会直接影响启动速度）
            ],
        }
    }


})
