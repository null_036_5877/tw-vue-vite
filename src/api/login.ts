/*
 * @Description:
 * @Version:
 * @Autor: LiChuang
 * @Date: 2022-06-29 16:19:20
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-06-29 16:33:21
 */
// @ts-ignore
import request from '@/utils/request'


//----------------------------- 获取表格数据------------------------
// 获取表格数据
export function getTableData(query: Object) {
    return request({ url: '/posts', method: 'get', params: query })
}

// 获取表格数据
export function login() {
    return request({ url: '/login', method: 'get' })
}
