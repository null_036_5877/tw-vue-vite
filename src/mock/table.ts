/*
 * @Description:
 * @Version:
 * @Autor: LiChuang
 * @Date: 2022-06-29 16:22:49
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-06-29 16:33:09
 */
// @ts-ignore
import { Result } from "./_utils"
import { MockMethod } from "vite-plugin-mock"
import Mock from "mockjs";

export default [
    {
        url: '/api/tableList', // 注意，这里只能是string格式
        method: 'get',
        response: () => {
            let rows = Mock.mock({
                'rows|20': [ // 生成一个 length是 1~100之间的数组list
                  {
                    'id': '@increment(1)', // id递增
                    'address': '@county', // 生成随机省
                    'age': '@integer(18,30)', // 生成18至30的整数
                    'avatar': '@image("300x250","#ff0000","#fff","gif","坤坤")', // 生成图片
                    'createTime': '@date(yyyy-MM-dd)', // 生成整改时间
                    'gender|0-1':'0',
                    'idCard':'@string(18)',
                    'status|1': ['published', 'draft', 'deleted'], // 状态,
                    'username':'@cname'
                  }
                ]})
            Result.data = {
                ...rows, code: 0
            }
            return Result;
        },
    },
] as MockMethod[]; // 定义数据格式
