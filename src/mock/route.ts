// @ts-ignore
import {Result} from "./_utils"
import {MockMethod} from "vite-plugin-mock"
import Mock from "mockjs";

import {
    Document,
    Menu as IconMenu,
    Location,
    Setting,
    Edit, User
} from '@element-plus/icons-vue'

export default [
    {
        url: '/api/getMenu', // 注意，这里只能是string格式
        method: 'get',
        response: () => {
            Result.data = [
                {
                    name: 'Homes',
                    level: 1,
                    component: 'layout',
                    meta: {title: '首页', name: 'Home', icon: 'iconshouye', affix: true},
                    path: '/',
                    children: [
                        {
                            parentId: 'Homes',
                            name: 'Home',
                            title: '首页',
                            meta: {title: '首页', name: 'Home', icon: 'iconshouye', affix: true},
                            path: '/',
                        }
                    ]
                },
                {
                    name: 'Users',
                    level: 1,
                    meta: {title: '用户', icon: 'iconziyuan111'},
                    component: 'layout',
                    path: '/users',
                    children: [
                        {
                            parentId: 'Users',
                            name: 'User',
                            meta: {title: '用户', name: 'Users', icon: 'iconziyuan111'},
                            path: '/users',
                        },
                        {
                            parentId: 'Users',
                            name: 'UserDetail',
                            meta: {title: '用户详情', name: 'UserDetail', icon: 'iconshujuquanxian'},
                            path: '/users/userDetails',
                        }
                    ]
                },
                {
                    name: 'Posts',
                    level: 1,
                    title: '岗位',
                    meta: {title: '岗位', name: 'Home', icon: 'iconshouye', affix: true},
                    component: 'layout',
                    path: '/post',
                    children: [
                        {
                            parentId: 'Posts',
                            name: 'Post',
                            meta: {title: '岗位', name: 'Posts', icon: 'iconqudaoguanli'},
                            path: '/post',
                        }
                    ]
                },
                {
                    level: 1,
                    name: 'Elements', component: 'layout',
                    meta: {title: 'ElementPlus', name: 'ECharts', icon: 'iconzhexiantu'},
                    path: '/element', children: [
                        {
                            parentId: 'Elements',
                            name: 'Element',
                            meta: {title: 'ElementPlus', name: 'UI Element', icon: 'iconkaifangpingtai'},
                            path: '/element'
                        },
                        {
                            parentId: 'Elements',
                            name: 'table',
                            level: 2,
                            meta: {title: 'ElementTable', name: 'ECharts', icon: 'iconzhexiantu'},
                            path: '/element/table',
                        },
                    ]
                },
                {
                    level: 1, name: 'forms', component: 'layout',
                    path: '/form', children: [
                        {
                            parentId: 'forms',
                            name: 'form-基础渲染',
                            meta: {title: 'form-基础渲染', name: 'form-基础渲染', icon: 'iconkaifangpingtai'},
                            path: '/form',
                        }
                    ]
                },
                {
                    level: 1, name: 'cesiums', component: 'layout',
                    title: 'cesiums',
                    meta: {title: 'cesiums', name: 'cesiums', icon: 'iconshouye', affix: true},
                    path: '/gis', children: [
                        {
                            parentId: 'cesiums',
                            name: 'gisCesium',
                            meta: {title: 'gis-cesium', name: 'gisCesium', icon: 'iconkaifangpingtai'},
                            path: '/gis/gisCesium',
                        },
                        {
                            parentId: 'cesiums',
                            name: 'cesiumChina',
                            meta: {title: 'cesium-china', name: 'cesiumChina', icon: 'iconkaifangpingtai'},
                            path: '/gis/cesiumChina',
                        }
                    ]
                },
                {
                    level: 1, name: 'three', component: 'layout',
                    title: 'three',
                    meta: {title: 'three', name: 'three', icon: 'iconshouye', affix: true},
                    path: '/three', children: [
                        {
                            parentId: 'three',
                            name: 'threeModel',
                            meta: {title: 'threeModel', name: 'threeModel', icon: 'iconkaifangpingtai'},
                            path: '/three/threeModel',
                        },
                        {
                            parentId: 'three',
                            name: 'threeCtiy',
                            meta: {title: 'threeCtiy', name: 'threeCtiy', icon: 'iconkaifangpingtai'},
                            path: '/three/threeCtiy',
                        },
                        // {
                        //     parentId: 'three',
                        //     name: 'threeChina',
                        //     meta: {title: 'threeChina', name: 'threeChina', icon: 'iconkaifangpingtai'},
                        //     path: '/three/threeChina',
                        // },
                    ]
                },
                // {id: 3, level: 1, meta: {title: '请求', name: 'Request', icon: 'iconsousuo'}, path: '/request',},
                {
                    name: 'Editors', component: 'layout', level: 1, path: '/edit', children: [
                        {
                            parentId: 'Editors',
                            name: 'Editor',
                            meta: {title: '编辑器', name: 'Editor', icon: 'iconbianji'},
                            path: '/edit',
                        }
                    ]
                },
                {
                    name: 'Charts',
                    level: 1,
                    meta: {title: '图表', name: 'Charts', icon: 'iconzhexiantu'},
                    path: '/echrats',
                    component: 'layout',
                    children: [
                        {
                            parentId: 'Charts',
                            name: 'chart',
                            level: 2,
                            meta: {title: 'ECharts', name: 'ECharts', icon: 'iconzhexiantu'},
                            path: '/echrats',
                        },
                        {
                            parentId: 'Charts',
                            name: 'highCharts',
                            level: 2,
                            meta: {title: 'HighCharts', name: 'HighCharts', icon: 'iconzhexiantu'},
                            path: '/echrats/highCharts',
                        },
                        {
                            parentId: 'Charts',
                            name: 'recharts',
                            level: 2,
                            meta: {title: 'Recharts', name: 'Rechartst', icon: 'iconzhexiantu'},
                            path: '/echrats/recCharts',
                        }
                    ]
                },
                {
                    name: 'animation', component: 'layout', level: 1, path: '/animation', children: [
                        {
                            parentId: 'animation',
                            name: 'skinPeeler',
                            meta: {title: '换肤动画', name: 'skinPeeler', icon: 'iconbianji'},
                            path: '/animation/skinPeeler',
                        }
                    ]
                },
            ]
            return Result;
        },
    },
]; // 定义数据格式

