/*
 * @Description:
 * @Version:
 * @Autor: LiChuang
 * @Date: 2022-06-29 16:22:49
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-06-29 16:33:09
 */
// @ts-ignore
import { MockMethod } from 'vite-plugin-mock'
import login from './login';
import route from './route';

// const modulesFiles = import.meta.globEager('./*.ts')
// const modules:any = {};
// for(let key in modulesFiles){
//     if (Object.prototype.hasOwnProperty.call(modulesFiles, key)) {
//         modules[key.replace(/^\.\/(.*)\.\w+$/, '$1')] = modulesFiles[key].default
//     }
// }
// console.log(modules);
export default [...login, ...route] as MockMethod[]; // 定义数据格式
