/*
 * @Description:
 * @Version:
 * @Autor: LiChuang
 * @Date: 2022-06-29 16:19:20
 * @LastEditors:
 * @LastEditTime: 2022-06-29 16:29:31
 */
import { useRoute, useRouter,createWebHashHistory, createRouter} from 'vue-router'
import Layout from '@/layout/index.vue'
export const constantRoutes = [
    {
        path: '/login',
        component: () => import('@/pages/login/index.vue'),
        hidden: true,
    },
    {
        path: '/404',
        component: () => import('@/pages/error-page/404.vue'),
        hidden: true,
    },
    // {
    //     path: "/:catchAll(.*)", // 不识别的path自动匹配404
    //     redirect: '/404',
    // },
    // {
    //     path: '/401',
    //     component: () => import('@/views/error-page/401'),
    //     hidden: true,
    // },
    {
        path: '/',
        component: Layout,
        redirect: '/dashboard',
        name: 'BasicLayout',
        children: [
            {
                path: '/',
                component: () => import('@/pages/home/index.vue'),
                name: 'home',
                hidden: true,
                meta: {
                    title: '首页',
                    icon: 'home',
                    affix: true,
                    local: true,
                    isContent: true,
                },
            },
            {
                path: '/users',
                component: () => import('@/pages/users/index.vue'),
                name: 'users',
                hidden: true,
                meta: {
                    title: '用户',
                    icon: 'users',
                    affix: true,
                    local: true,
                    isContent: true,
                },
            },
        ],
    },
]

export const router = createRouter({
    history: createWebHashHistory(),
    routes: constantRoutes
})

// console.log(router)
export default router
