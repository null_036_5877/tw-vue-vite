import { createStore } from 'vuex'
import getters from './getters'


// const modulesFiles = require.context('./modules', true, /\.js$/)
const modulesFiles = import.meta.globEager('./modules/*.ts')
// console.log(modulesFiles)
const modules:any = [];
for(let key in modulesFiles){
    if (Object.prototype.hasOwnProperty.call(modulesFiles, key)) {
        let valKey = key.replace(/modules\//, '')
        modules[valKey.replace(/^\.\/(.*)\.\w+$/, '$1')] = modulesFiles[key].default
    }
}
export default createStore({
  getters,
  modules
})
