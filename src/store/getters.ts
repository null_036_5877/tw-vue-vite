const getters = {
    userId: (state:{user: { userId: string }}) => state.user.userId,
    menus: (state:{menus: { menus: any } }) => state.menus.menus,
    isCollapse: (state:{menus: { isCollapse: boolean } }) => state.menus.isCollapse,
}
export default getters
