// import { login, getInfo, loginOut } from '@/api/auth'
// @ts-ignore
import { getToken, setToken, removeToken } from "@/utils/auth"
// @ts-ignore
import { login } from '@/api/login'
// @ts-ignore
import router from "@/router"

// import Layout from '@/layout'
// import router from '@/router'

const state = {
    userId: null,
    user: {},
    token: '',
}

const mutations = {

}

const actions = {
    login({commit}: any, ruleForm: any){
        // const { loginName, loginPassword, loginType } = userInfo
        return new Promise((resolve,reject)=>{
            login(ruleForm).then((res: any)=>{
                resolve(res)
                setToken(res.token)
                router.push('/')
            }).catch((err: any)=>{
                reject()
            })
        })
    },
    logout({commit}: any){
        removeToken()
        router.push('/login')
    },
}
export default {
    namespaced: true,
    state,
    mutations,
    actions
}
