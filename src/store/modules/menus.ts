// @ts-ignore
import { getMenu } from '@/api/common.ts'
// @ts-ignore
import router from '@/router'

const state = {
    menus: [],
    isCollapse: false
}

const mutations = {
    SET_MENUS: (state:any, data: void) => {
        state.menus = data
    },
    SET_ISCOLLAPSE: (state:any, isCollapse: boolean) => {
        state.isCollapse = isCollapse
    },
}

const actions = {
    // 获取菜单数据
    getMenus({commit}: any, ruleForm: any){
        return new Promise((resolve,reject)=>{
            getMenu(ruleForm).then((res: any)=>{
                commit('SET_MENUS', res)
                if (res) {
                    routerPackag(res)
                }
                resolve(res)
            }).catch((err: any)=>{
                reject()
            })
        })
    },
    // 菜单显示状态
    setIsCollapse({commit}: any, status: boolean){
        commit('SET_ISCOLLAPSE', status)
    }
}

import Layout from '@/layout/index.vue'
/**
 * 请不要随便更改当前代码
 * */
const routerPackag = (routers: any[]) => {
    routers.filter((itemRouter) => {
        if (itemRouter.component != 'layout') {
            if (['','/'].includes(itemRouter.path)) {
                itemRouter['component'] = () => defineAsyncComponent(() =>import('@/pages/home/index.vue'))
            } else {
                itemRouter['component'] = () => defineAsyncComponent(() =>import(`../../pages${itemRouter.path}/index.vue`))
            }
        } else {
            itemRouter['component'] = Layout
        }
        if (itemRouter.level == 1) {
            router.addRoute(itemRouter)
        } else {
            router.addRoute(itemRouter.parentId, itemRouter)
        }
        if (itemRouter.children && itemRouter.children.length) {
            routerPackag(itemRouter.children)
        }
        // return true
    })
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
