import router from './router'
// import {useStore} from "@/store/index"
// @ts-ignore
import store from './store/index.ts'
// @ts-ignore
import NProgress from "nprogress" // 进度条
import 'nprogress/nprogress.css' // 进度条样式
// @ts-ignore
import {getToken} from '@/utils/auth'


NProgress.configure({showSpinner: false})

const whiteList = ['/login', '/auth-redirect']

/**
 * 自定义配置不需要请求table配置信息页面
 * */
let arr = ['/login', '/dashboard', '/typePage', '/editPassword', '/userInfo', '/setUp/set/enterpriseDetails']


router.beforeEach(async (to, from, next) => {
    NProgress.start()
    const hasToken = getToken()
    if (hasToken) {
        if (to.path === '/login') {
            next({path: '/'})
            NProgress.done()
        } else {
            let menus = store.getters.menus
            if (menus.length == 0) {
                // if (to.meta.local) {
                //     next()
                //     NProgress.done()
                //     return
                // }
                await store.dispatch('menus/getMenus')
            }
            // 白名单
            if(to.name){
                if (!arr.includes(to.path)) {
                    // await store.dispatch('getTableLists/getTableData', to.name)
                }
                next()
            } else{
                // 有token，没路由地址，直接跳转首页
                next({...to, replace: true})
                // next('/')
            }
            NProgress.done()
        }
    } else {
        if (whiteList.indexOf(to.path) !== -1) {
            next()
        } else {
            next(`/login?redirect=${to.path}`)
            NProgress.done()
        }
    }
})

router.afterEach(() => {
    NProgress.done()
})
