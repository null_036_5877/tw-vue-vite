/*
 * @Description:
 * @Version:
 * @Autor: LiChuang
 * @Date: 2022-06-29 16:19:20
 * @LastEditors: Please set LastEditors
 * @LastEditTime: 2022-06-29 16:35:31
 */
// @ts-ignore
import axios from 'axios'
// @ts-ignore
import twVueEl from 'tw-vue-el'
// import store from '@/store'
// import { getToken } from '@/utils/auth'
// @ts-ignore
import {Constant} from '@/mock/_utils'


// console.log(twVueEl);
// console.log(twVueEl.ElMessage({message: 'Error', type: 'error', duration: 5000, showClose: true}));
const {ApiPrefix} = Constant
const service = axios.create({
    // baseURL: process.env.VUE_APP_BASE_API,
    baseURL: '/api',
    timeout: 30000,
})
service.interceptors.request.use(
    (config: any) => {
        //企业平台：1  管理平台：0
        // config.headers['system'] = 1
        // if (store.getters.token) {
        //   config.headers['Authorization'] = getToken()
        // }

        return config
    },
    (error: any) => {
        return Promise.reject(error)
    }
)

// response interceptor
service.interceptors.response.use(
    (response: { data: any }) => {
        const res = response.data
        const code = res.code
        if (code === 200 || (code >= 600 && code < 700)) {
            return res.data
        } else if (code === 401 || [50008, 50012, 50014, 4033].includes(res.resp_code)) {
            // ElMessage({message: res.resp_msg || 'Error', type: 'warning', duration: 5 * 1000, showClose: true,
            //   onClose: function (){
            //     store.dispatch('user/resetToken').then(() => {
            //       location.reload()
            //     })
            //   }
            // })
            return Promise.reject(res.resp_msg || 'Error')
        } else {
            ElMessage({message: res.resp_msg || 'Error', type: 'error', duration: 5 * 1000, showClose: true})
            return Promise.reject(res.resp_msg || 'Error')
        }
    },
    (error: { message: number[] }) => {
        let type = 500
        let msgObj = {
            400: '请检查请求参数',
            401: '请先登录',
            403: '没有权限，请联系管理员',
            500: '服务器内部错误',
            502: '网关错误',
            504: '请求超时',
        }
        if (error.message.indexOf(500) > -1) {
            type = 500
        } else if (error.message.indexOf(400) > -1) {
            type = 400
        } else if (error.message.indexOf(401) > -1) {
            // store.dispatch('user/resetToken').then(() => {
            //     location.reload()
            // })
            type = 401
        } else if (error.message.indexOf(403) > -1) {
            type = 403
        } else if (error.message.indexOf(502) > -1) {
            type = 502
        } else if (error.message.indexOf(504) > -1) {
            type = 504
        }
        // @ts-ignore
        ElMessage({message: msgObj[type], type: 'error', duration: 5 * 1000, showClose: true})
        return Promise.reject(error)
    }
)

export default service
