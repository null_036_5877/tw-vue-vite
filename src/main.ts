import { createApp } from "vue"
import App from './App.vue'
import router from "./router/index"
// import ElementPlus from 'element-plus'
// import 'element-plus/dist/index.css'
import '@/styles/index.scss'
import "./permission"
import store from './store/index'
import '@/assets/font/iconfont.js';
// import zhCn from 'element-plus/es/locale/lang/zh-cn'
import twVueEl from 'tw-vue-el'
import 'tw-vue-el/dist/style.css'

const app = createApp(App)

// console.log(twVueEl);
// console.log(ElementPlus);


// app.use(ElementPlus,{locale: zhCn})
app.use(router)
app.use(twVueEl)
app.use(store)
app.mount('#app')
